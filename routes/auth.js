const router = require('express').Router();

module.exports = (app) => {
    // LOGIN - GET A NEW TOKEN
    router.post('/login',
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.login,
        app.actions.auth.login
    );

    // LOGOUT - DELETE ALL USER TOKENS
    router.get('/logout',
        app.middlewares.security.isAuthenticated,
        app.actions.auth.logout
    );

    // REGISTER - CREATE NEW PROFILE
    router.post('/register',
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.user,
        app.actions.auth.register
    );
    
    return router;
};
