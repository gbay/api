const router = require('express').Router();

module.exports = (app) => {
    // FINDALL - GET ALL AUCTIONS
    router.get('/',
        // app.middlewares.tools.cache.get,
        app.actions.auctions.findAll
    );

    // FINDONE - GET ONE AUCTION
    router.get('/:auctionId',
        // app.middlewares.tools.cache.get,
        app.middlewares.parsers.auction,
        app.actions.auctions.findOne
    );

    // CREATE - NEW AUCTION
    router.post('/',
        app.middlewares.security.isAuthenticated,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.auction,
        app.actions.auctions.create
    );

    // CREATE - NEW AUCTION BID
    router.post('/:auctionId/bids',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.auction,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.bid,
        app.middlewares.security.isNotAuctionOwner,
        app.middlewares.security.canBid,
        app.actions.auctions.bids.create
    );

    return router;
};
