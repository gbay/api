module.exports = (app) => {
    console.log('Loading routes...\n');

    app.use('/auctions', require('./auctions')(app));
    app.use('/auth', require('./auth')(app));
    app.use('/categories', require('./categories')(app));
    app.use('/products', require('./products')(app));
    app.use('/sells', require('./sells')(app));
    app.use('/sellers', require('./sellers')(app));
    app.use('/users', require('./users')(app));
};
