const router = require('express').Router();

module.exports = (app) => {
    // FINDALL - GET ALL SELLERS
    router.get('/',
        // app.middlewares.tools.cache.get,
        app.actions.sellers.findAll
    );

    // FINDONE - GET ONE SELLER
    router.get('/:sellerId',
        // app.middlewares.tools.cache.get,
        app.middlewares.parsers.seller,
        app.actions.sellers.findOne
    );

    // CREATE - NEW SELLER EVALUATION
    router.post('/:sellerId/evaluations',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.seller,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.evaluation,
        app.middlewares.security.isClient,
        app.middlewares.security.hasNotEvaluated,
        app.actions.sellers.evaluations.create
    );

    // REMOVE - DELETE SELLER EVALUATION
    router.delete('/:sellerId/evaluations/:buyerId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.seller,
        app.middlewares.security.isEvaluationOwner,
        app.actions.sellers.evaluations.remove
    );

    // UPDATE - EDIT SELLER EVALUATION
    router.put('/:sellerId/evaluations/:buyerId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.seller,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.evaluation,
        app.middlewares.security.isEvaluationOwner,
        app.actions.sellers.evaluations.update
    );

    return router;
};
