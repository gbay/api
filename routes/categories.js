const router = require('express').Router();

module.exports = (app) => {
    // FINDALL - GET ALL CATEGORIES
    router.get('/',
        // app.middlewares.tools.cache.get,
        app.actions.categories.findAll
    );

    return router;
};
