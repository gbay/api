const router = require('express').Router();

module.exports = (app) => {
    // FINDONE - GET USER PROFILE
    router.get('/:userId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.security.isAccountOwner,
        // app.middlewares.tools.cache.get,
        app.actions.users.findOne
    );

    // UPDATE - EDIT USER PROFILE
    router.put('/:userId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.security.isAccountOwner,
        app.middlewares.bodyparser.json(),
        app.actions.users.update
    );

    // REMOVE - DELETE USER PROFILE
    router.delete('/:userId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.security.isAccountOwner,
        app.actions.users.remove
    );

    // CREDITS - CREDIT USER ACCOUNT
    router.post('/:userId/credits',
        app.middlewares.security.isAuthenticated,
        app.middlewares.security.isAccountOwner,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.credits,
        app.actions.users.credits
    );

    // PURCHASES - FIND ALL USER PURCHASES
    router.get('/:userId/purchases',
        app.middlewares.security.isAuthenticated,
        app.middlewares.security.isAccountOwner,
        // app.middlewares.tools.cache.get,
        app.actions.users.purchases.findAll
    );

        // PURCHASES - FIND ONE USER PURCHASE
    router.get('/:userId/purchases/:purchaseId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.security.isAccountOwner,
        // app.middlewares.tools.cache.get,
        app.actions.users.purchases.findOne
    );

    return router;
};
