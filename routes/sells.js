const router = require('express').Router();

module.exports = (app) => {
    // FINDALL - GET ALL SELLS
    router.get('/',
        app.middlewares.security.isAuthenticated,
        app.middlewares.security.isRole('Administrateur'),
        // app.middlewares.tools.cache.get,
        app.actions.sells.findAll
    );

    // FINDONE - GET ONE SELL
    router.get('/:sellId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.security.isRole('Administrateur'),
        app.middlewares.parsers.sell,
        // app.middlewares.tools.cache.get,
        app.actions.sells.findOne
    );

    // CREATE - NEW SELL FEEDBACK
    router.post('/:sellId/feedbacks',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.sell,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.feedback,
        app.middlewares.security.isBuyer,
        app.actions.sells.feedbacks.create
    );

    // UPDATE - EDIT SELL FEEDBACK
    router.put('/:sellId/feedbacks',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.sell,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.feedback,
        app.middlewares.security.isBuyer,
        app.actions.sells.feedbacks.update
    );

    // REMOVE - DELETE SELL FEEDBACK
    router.delete('/:sellId/feedbacks',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.sell,
        app.middlewares.security.isBuyer,
        app.actions.sells.feedbacks.remove
    );

    return router;
};
