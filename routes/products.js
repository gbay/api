const router = require('express').Router();

module.exports = (app) => {
    // FINDALL - GET ALL PRODUCTS
    router.get('/',
        // app.middlewares.tools.cache.get,
        app.actions.products.findAll
    );

    // FINDONE - GET ONE PRODUCT
    router.get('/:productId',
        // app.middlewares.tools.cache.get,
        app.middlewares.parsers.product,
        app.actions.products.findOne
    );

    // CREATE - NEW PRODUCT
    router.post('/',
        app.middlewares.security.isAuthenticated,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.product,
        app.actions.products.create
    );

    // UPDATE - EDIT ONE PRODUCT
    router.put('/:productId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.product,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.product,
        app.middlewares.security.isProductOwner,
        app.actions.products.update
    );

    // REMOVE - DELETE ONE PRODUCT
    router.delete('/:productId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.product,
        app.middlewares.security.isProductOwner,
        app.actions.products.remove
    );

    // BUY - DIRECT BUY PRODUCT
    router.post('/:productId/buy',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.product,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.buy,
        app.middlewares.security.isNotProductOwner,
        app.middlewares.security.canDirectBuy,
        app.actions.products.buy
    );

    return router;
};
