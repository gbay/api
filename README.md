### Install ###

* Clone repository: git clone https://bitbucket.org/bmyguest/gbay-api.git
* Go to project folder: cd gbay-api
* Install dependencies: npm install
* Start mysql: service mysql start
* Start mongodb: mongod
* Config mysql/mongodb passwords if needed: edit settings/settings.json (optional)
* Start project: node .
* Let the magic happen

### Presentation ###

https://prezi.com/k6bdg-slsnop/gbay/?utm_campaign=share&utm_medium=copy

### TODO ###

Good job ! Nothing left to do :)

### API Endpoints ###

* [GET] /auctions
* [GET] /auctions/:auctionId
* [POST] /auctions
* [POST] /auctions/:auctiondId/bids

* [POST] /auth/login
* [GET] /auth/logout
* [POST] /auth/register

* [GET] /categories

* [GET] /products
* [GET] /products/:productId
* [POST] /products
* [PUT] /products/:productId
* [DELETE] /products/:productId
* [GET] /products/:productId/buy

* [GET] /sellers
* [GET] /sellers/:sellerId
* [POST] /sellers/:sellerId/evaluations
* [DELETE] /sellers/:sellerId/evaluations/:buyerId
* [PUT] /sellers/:sellerId/evaluations/:buyerId

* [GET] /sells
* [GET] /sells/:sellId
* [POST] /sells/:sellId/feedbacks
* [PUT] /sells/:sellId/feedbacks
* [DELETE] /sells/:sellId/feedbacks

* [GET] /users/:userId
* [PUT] /users/:userId
* [DELETE] /users/:userId
* [POST] /users/:userId/credits
* [GET] /users/:userId/purchases
* [GET] /users/:userId/purchases/:purchaseId