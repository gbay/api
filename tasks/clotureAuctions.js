module.exports = (app) => {
    const Auction = app.models.Auction;
    const Bid = app.models.Bid;
    const Product = app.models.Product;
    const Sell = app.models.Sell;
    const User = app.models.User;

    setInterval(function() {
        Auction.findAll({
            where: { closedAt: null, deletedAt: null, end: { $lt: new Date() } },
            include: [ { model: Bid, include: [ { model: User, as: 'buyer' } ] }, { model: Sell, include: [ { model: Product, include: [ { model: User, as: 'seller' } ] } ] } ]
        }).then((auctions) => {
            auctions.forEach((auction) => {
                auction.closedAt = new Date();
                auction.save().then(() => {
                    auction.bids.forEach((bid) => {
                        if (bid.id !== auction.bids[auction.bids.length - 1].id) {
                            bid.buyer.increment({ credits: bid.amount});
                        } else {
                            auction.sell.setBuyer(bid.buyer);
                            bid.buyer.addPurchase(auction.sell);
                            auction.sell.product.seller.credits += bid.amount;
                            auction.sell.product.seller.save();

                            console.log(`Auction ${auction.id} closed at ${auction.closedAt}. Buyer: ${bid.buyer.name} (${bid.buyer.email})`);
                        }
                    });
                }).catch((err) => {
                    console.log(err.message);
                });
            })
        }).catch((err) => {
            console.log(err.message);
        });
    }, 5000);
};
