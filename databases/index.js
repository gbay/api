const Promise = require('bluebird').Promise;

module.exports = (app) => {
    console.log('Loading databases...');

    return new Promise((resolve, reject) => {
        require('./mongodb')(app);
        require('./mysql')(app, resolve, reject);
    });
};