const Sequelize = require('sequelize');
const mysql = require('mysql');

module.exports = (app, resolve, reject) => {
        const url = `mysql://${app.settings.mysql.user}:${app.settings.mysql.password}@${app.settings.mysql.host}:${app.settings.mysql.port}/${app.settings.mysql.database}`;
        const rootUrl = `mysql://${app.settings.mysql.rootUser}:${app.settings.mysql.rootPassword}@${app.settings.mysql.host}:${app.settings.mysql.port}/`;

        let initMySQL = (err) => {
            if (err) {
                console.log(`MySQL ${err}`);

                if (err.code === 'ECONNREFUSED') {
                    console.log(`Connecting Sequelize to MySQL on ${app.settings.mysql.user}@${app.settings.mysql.host}:${app.settings.mysql.port}/${app.settings.mysql.database}...FAILED`);
                    return reject('Cannot connect to MySQL');
                }

                const rootConnection = mysql.createConnection(rootUrl);
                process.stdout.write(`MySQL (root): try creating database ${app.settings.mysql.database}...`);

                rootConnection.query(`CREATE DATABASE IF NOT EXISTS ${app.settings.mysql.database}`, (err) => {
                    if (err) return reject('MySQL ', err);

                    process.stdout.write(`MySQL (root): try creating user ${app.settings.mysql.user}@${app.settings.mysql.host}/${app.settings.mysql.database}...`);

                    rootConnection.query(`GRANT SELECT, INSERT, DELETE, UPDATE, EXECUTE, CREATE, ALTER, DROP, INDEX, REFERENCES ON ${app.settings.mysql.database}.* To '${app.settings.mysql.user}'@'${app.settings.mysql.host}' IDENTIFIED BY '${app.settings.mysql.password}'`, (err) => {
                        if (err) return reject('MySQL ', err);

                        rootConnection.end();
                        initSequelize();
                    });
                });
            } else if (!err) {
                initSequelize();
            }
        };

        function initSequelize() {
            app.sequelize = new Sequelize(url, { logging: false });
            app.sequelize.authenticate()
                .then(() => { console.log(`Connecting Sequelize to MySQL on ${app.settings.mysql.user}@${app.settings.mysql.host}:${app.settings.mysql.port}/${app.settings.mysql.database}...`); resolve(); })
                .catch((err) => { console.log('MySQL ', err); });
        }

        mysql.createConnection(url).connect(initMySQL);
};
