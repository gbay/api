const mongoose = require('mongoose');

module.exports = (app) => {
    const url = `mongodb://${app.settings.mongodb.user}:${app.settings.mongodb.password}@${app.settings.mongodb.host}:${app.settings.mongodb.port}/${app.settings.mongodb.database}`;

    let onSuccess = () => {
        app.mongoose = mongoose;
        console.log(`Connecting Mongoose to MongoDB on ${app.settings.mongodb.user}@${app.settings.mongodb.host}:${app.settings.mongodb.port}/${app.settings.mongodb.database}...`);
    };

    let onError = (err) => {
        console.log(`MongoDB ERROR: ${err}`);
    };

    mongoose.Promise = require('bluebird');
    mongoose.connect(url).then(onSuccess).catch(onError);
};