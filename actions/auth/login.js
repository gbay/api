const sha1 = require('sha1');
const jwt = require('jsonwebtoken');

module.exports = (app) => {
    const User = app.models.User;
    const Token = app.models.Token;

    return function login(req, res, next) {
        User.findOne({
            where: { deletedAt: null, email: req.body.email, password: sha1(req.body.password)}
        }).then((user) => {
            if(!user) return res.status(401).send('Invalid credentials');

            let token = new Token();
            token.userId = user.id;

            token.save((err, data) => {
                if(err) return res.status(500).send(err);
                if (!data) return res.sendStatus(201);

                jwt.sign({ tokenId : token.id }, app.settings.security.salt, { expiresIn: '1d' }, (err, encryptedToken) => {
                    if(err) return res.status(500).send(err);
                    res.send({ userId: user.id, token:encryptedToken });
                });
            });
        }).catch((err) => {
            return res.status(500).send(err.message);
        });
    }
};
