module.exports = (app) => {
    const Token = app.models.Token;

    return function logout(req, res, next) {
        Token.remove({ userId: req.user.id }, (err, data) => {
            if (err) return res.status(500).send(err);
            if (!data) return res.sendStatus(201);
            res.send(data);
        });
    }
};
