const sha1 = require('sha1');

module.exports = (app) => {
    const User = app.models.User;

    return function register(req, res, next) {
        req.body.password = sha1(req.body.password);

        User.findOrCreate({
            where: { email: req.body.email },
            defaults: { password: req.body.password, name: req.body.name, credits: 0.0, roleId: 2 }
        }).spread((user, created) => {
            if (created) return res.send(user);
            return res.status(401).send('Email already in use.');
        });
    }
};