module.exports = (app) => {
    return {
        findOne: require('./findOne')(app),
        update: require('./update')(app),
        remove: require('./remove')(app),
        credits: require('./credits')(app),
        purchases: require('./purchases')(app)
    };
};
