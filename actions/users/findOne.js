module.exports = (app) => {
    return function findOne(req, res, next) {
        app.middlewares.tools.cache.set('User', req.user, req.originalUrl);
        res.send(req.user);
    }
};
