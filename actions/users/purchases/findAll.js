module.exports = (app) => {
    const Auction = app.models.Auction;
    const Product = app.models.Product;
    const Feedback = app.models.Feedback;
    const User = app.models.User;

    return function findAll(req, res, next) {
        req.user.getPurchases({
            include: [ { model: Auction }, { model: Product, include: [ { model: User, as: 'seller' } ] }, { model: Feedback, required: false } ]
        }).then((purchases) => {
            res.send(purchases);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
