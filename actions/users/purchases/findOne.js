module.exports = (app) => {
    const Auction = app.models.Auction;
    const Product = app.models.Product;
    const Feedback = app.models.Feedback;
    const User = app.models.User;

    return function findOne(req, res, next) {
        req.user.getPurchases({
            where: { id: req.params.purchaseId },
            include: [ { model: Auction }, { model: Product, include: [ { model: User, as: 'seller' } ] }, { model: Feedback, required: false } ]
        }).then((purchases) => {
            if (!purchases.length) return res.status(404).send('Purchase not found');
            res.send(purchases[0]);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
