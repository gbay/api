module.exports = (app) => {
    const Token = app.models.Token;

    return function remove(req, res, next) {
        req.user.destroy().then((user) => {
            Token.remove({ userId: req.user.id }, (err, data) => {
                if (err) return res.status(500).send(err);
                if (!data) return res.sendStatus(201);
                app.middlewares.tools.cache.clean('User');
                res.send(user);
            });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};