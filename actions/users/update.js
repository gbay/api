const sha1 = require('sha1');

module.exports = (app) => {
    const Token = app.models.Token;

    return function update(req, res, next) {
        req.user.email = req.body.email ? req.body.email : req.user.email;
        req.user.password = req.body.password ? sha1(req.body.password) : req.user.password;
        req.user.name = req.body.name ? req.body.name : req.user.name;

        req.user.save().then((data) => {
            app.middlewares.tools.cache.clean('User');

            Token.remove({ userId: req.user.id }, (err) => {
                if (err) return res.status(500).send(err);
                res.send(data);
            });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};