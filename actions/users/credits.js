module.exports = (app) => {
    return function credits(req, res, next) {
        req.user.credits += Number(req.body.credits);

        req.user.save().then((data) => {
            app.middlewares.tools.cache.clean('User');
            res.send(data);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};