module.exports = (app) => {
    const User = app.models.User;
    const Product = app.models.Product;
    const Evaluation = app.models.Evaluation;
    const Sell = app.models.Sell;
    const Feedback = app.models.Feedback;

    return function findAll(req, res, next) {
        User.findAll({
            where: { deletedAt: null },
            include: [ { model: Product, required: true, include: [ { model: Sell, required: false, include: [ { model: Feedback, required: false, where: { deletedAt: null }  } ] } ] }, { model: Evaluation, required: false, where: { deletedAt: null } } ]
        }).then((sellers) => {
            if (!sellers) return res.sendStatus(204);

            let sellersJSON = [];
            let index = 0;

            sellers.forEach((seller) => {
                let feedbackCount = 0;
                let noteSum = 0;

                seller.products.forEach((product) => {
                    product.sells.forEach((sell) => {
                        if (sell.feedback) {
                            feedbackCount++;
                            noteSum += sell.feedback.note;
                        }
                    });
                });

                sellersJSON.push(seller.toJSON());
                sellersJSON[index].note = feedbackCount > 0 ? noteSum / feedbackCount : null;
                index++;
            });

            app.middlewares.tools.cache.set('Seller', sellers, req.originalUrl);
            res.send(sellersJSON);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
