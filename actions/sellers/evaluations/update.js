module.exports = (app) => {
    return function update(req, res, next) {
        req.seller.getEvaluations({ where: { buyerId: req.user.id } }).then((evaluations) => {
            evaluations[0].comment = req.body.comment ? req.body.comment : evaluations[0].comment;

            evaluations[0].save().then((data) => {
                app.middlewares.tools.cache.clean('Seller');
                res.send(data);
            }).catch((err) => {
                res.status(500).send(err.message);
            });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};