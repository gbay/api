module.exports = (app) => {
    const Evaluation = app.models.Evaluation;

    return function create(req, res, next) {
        Evaluation.build(req.body).save().then((evaluation) => {
            app.middlewares.tools.cache.clean('Seller');
            evaluation.setBuyer(req.user);
            req.seller.addEvaluation(evaluation);
            res.send(evaluation);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};