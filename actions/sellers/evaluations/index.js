module.exports = (app) => {
    return {
        create: require('./create')(app),
        remove: require('./remove')(app),
        update: require('./update')(app)
    };
};