module.exports = (app) => {
    return function remove(req, res, next) {
        req.seller.getEvaluations({ where: { buyerId: req.user.id } }).then((evaluation) => {
            evaluation[0].destroy().then((evaluation) => {
                app.middlewares.tools.cache.clean('Seller');
                res.send(evaluation);
            }).catch((err) => {
                res.status(500).send(err.message);
            });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};