module.exports = (app) => {
    return function findOne(req, res, next) {
        app.middlewares.tools.cache.set('Seller', req.seller, req.originalUrl);
        req.seller = req.seller.toJSON();

        let feedbackCount = 0;
        let noteSum = 0;
        req.seller.products.forEach((product) => {
            product.sells.forEach((sell) => {
                if (sell.feedback) {
                    feedbackCount++;
                    noteSum += sell.feedback.note;
                }
            });
        });

        req.seller.note = feedbackCount > 0 ? noteSum / feedbackCount : null;
        res.send(req.seller);
    }
};
