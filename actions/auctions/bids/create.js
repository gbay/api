module.exports = (app) => {
    const Bid = app.models.Bid;

    return function create(req, res, next) {
        Bid.build(req.body).save().then((bid) => {
            req.user.credits -= bid.amount;
            req.user.save().then((user) => {
                req.user.addBid(bid);
                req.auction.addBid(bid);
                app.middlewares.tools.cache.clean('Auction');
                res.send(bid);
            }).catch((err) => {
                res.status(500).send(err.message);
            });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};