module.exports = (app) => {
    const Auction = app.models.Auction;
    const Bid = app.models.Bid;
    const Product = app.models.Product;
    const Sell = app.models.Sell;
    const User = app.models.User;

    return function findAll(req, res, next) {
        Auction.findAll({
            where: { deletedAt: null, closedAt: null },
            include: [ { model: Bid, required: false, where: { deletedAt: null }, include: [ { model: User, as: 'buyer' } ] }, { model: Sell, include: [ { model: Product, include: [ { model: User, as: 'seller' } ] } ] } ]
        }).then((data) => {
            if (!data) return res.sendStatus(204);
            app.middlewares.tools.cache.set('Auction', data, req.originalUrl);
            res.send(data);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
