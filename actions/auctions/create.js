module.exports = (app) => {
    const Auction = app.models.Auction;
    const Sell = app.models.Sell;
    const Product = app.models.Product;

    return function create(req, res, next) {
        Product.findOne({
            where: { deletedAt: null, id: req.body.productId, sellerId: req.user.id, stock: { $gte: req.body.quantity } }
        }).then((product) => {
            if (!product) return res.status(404).send('Product not found, not your product or insufficient stock');

            Sell.build(req.body).save().then((sell) => {
                Auction.build(req.body).save().then((auction) => {
                    app.middlewares.tools.cache.clean('Auction');
                    auction.setSell(sell);
                    sell.setAuction(auction);
                    product.stock -= sell.quantity;
                    product.save();
                    res.send(auction);
                }).catch((err) => {
                    res.status(500).send(err.message);
                });
            }).catch((err) => {
                res.status(500).send(err.message);
            });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};