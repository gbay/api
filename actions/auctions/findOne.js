module.exports = (app) => {
    return function findOne(req, res, next) {
        app.middlewares.tools.cache.set('Auction', req.auction, req.originalUrl);
        res.send(req.auction);
    }
};
