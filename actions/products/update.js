module.exports = (app) => {
    return function update(req, res, next) {
        req.product.name = req.body.name ? req.body.name : req.product.name;
        req.product.price = req.body.price ? req.body.price : req.product.price;
        req.product.description = req.body.description ? req.body.description : req.product.description;
        req.product.stock = req.body.stock ? req.body.stock : req.product.stock;
        req.product.directBuy = req.body.directBuy ? req.body.directBuy : req.product.directBuy;
        req.product.setCategories(req.body.categories);

        req.product.save().then((data) => {
            app.middlewares.tools.cache.clean('Product');
            app.middlewares.tools.cache.clean('Seller');
            res.send(data);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};