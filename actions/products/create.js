module.exports = (app) => {
    const Product = app.models.Product;

    return function create(req, res, next) {
        Product.build(req.body).save().then((product) => {
            app.middlewares.tools.cache.clean('Product');
            app.middlewares.tools.cache.clean('Seller');
            product.setCategories(req.body.categories);
            req.user.addProduct(product);
            res.send(product);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};