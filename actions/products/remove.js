module.exports = (app) => {
    return function remove(req, res, next) {
        req.product.destroy().then((product) => {
            app.middlewares.tools.cache.clean('Product');
            app.middlewares.tools.cache.clean('Seller');
            res.send(product);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};