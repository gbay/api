module.exports = (app) => {
    return function findOne(req, res, next) {
        app.middlewares.tools.cache.set('Product', req.product, req.originalUrl);
        res.send(req.product);
    }
};
