module.exports = (app) => {
    return {
        create: require('./create')(app),
        findAll: require('./findAll')(app),
        findOne: require('./findOne')(app),
        update: require('./update')(app),
        remove: require('./remove')(app),
        buy: require('./buy')(app)
    };
};