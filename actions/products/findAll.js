module.exports = (app) => {
    const Product = app.models.Product;
    const Category = app.models.Category;
    const Sell = app.models.Sell;
    const Auction = app.models.Auction;
    const User = app.models.User;

    return function findAll(req, res, next) {
        let query = { where: { deletedAt: null }, order: [], include:[ { model: Category, as: 'categories' }, { model: User, as: 'seller' }, { model: Sell, required: false, where: { buyerId: null }, include: [ { model: Auction, required: false, where: { closedAt: null, deletedAt: null } } ] } ] };

        if (req.query.sellerId) {
            query.where.sellerId = req.query.sellerId;
        }

        if (req.query.order) {
            query.order.push([ req.query.order, 'DESC' ]);
        }

        if (req.query.categories) {
            query.include[0].where = { id: { $in: req.query.categories.split(',') } };
        }

        if (req.query.page) {
            let items = Math.min(req.query.items ? Number(req.query.items) : 3, 100);
            query.offset = Math.max(items * (req.query.page - 1), 0);
            query.limit = items;
        }

        Product.findAll(query).then((data) => {
            if (!data) return res.sendStatus(204);
            app.middlewares.tools.cache.set('Product', data, req.originalUrl);
            res.send(data);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
