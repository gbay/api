module.exports = (app) => {
    const Sell = app.models.Sell;

    return function buy(req, res, next) {
        Sell.build(req.body).save().then((sell) => {
            req.user.credits -= req.product.price * sell.quantity;
            req.user.save();
            req.user.addPurchase(sell);

            req.product.seller.credits += req.product.price * sell.quantity;
            req.product.seller.save();

            sell.setProduct(req.product);

            if (req.product.stock !== null) {
                req.product.stock -= sell.quantity;
                req.product.save();
            }

            app.middlewares.tools.cache.clean('Product');
            app.middlewares.tools.cache.clean('Seller');
            app.middlewares.tools.cache.clean('Sell');
            res.send(sell);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};