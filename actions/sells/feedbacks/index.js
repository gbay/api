module.exports = (app) => {
    return {
        create: require('./create')(app),
        update: require('./update')(app),
        remove: require('./remove')(app)
    }
};