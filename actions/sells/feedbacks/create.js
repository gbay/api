module.exports = (app) => {
    const Feedback = app.models.Feedback;

    return function create(req, res, next) {
        Feedback.build(req.body).save().then((feedback) => {
            app.middlewares.tools.cache.clean('Sell');
            feedback.setSell(req.sell);
            res.send(feedback);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};