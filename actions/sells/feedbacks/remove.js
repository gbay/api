module.exports = (app) => {
    return function remove(req, res, next) {
        req.sell.getFeedback().then((feedback) => {
            feedback.destroy().then((data) => {
                app.middlewares.tools.cache.clean('Sell');
                res.send(data);
            }).catch((err) => {
                res.status(500).send(err.message);
            });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};