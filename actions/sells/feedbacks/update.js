module.exports = (app) => {
    return function update(req, res, next) {
        req.sell.getFeedback().then((feedback) => {
            feedback.note = req.body.note ? req.body.note : feedback.note;
            feedback.comment = req.body.comment ? req.body.comment : feedback.comment;

            feedback.save().then((data) => {
                app.middlewares.tools.cache.clean('Sell');
                res.send(data);
            }).catch((err) => {
                res.status(500).send(err.message);
            });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};