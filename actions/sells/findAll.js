module.exports = (app) => {
    const Sell = app.models.Sell;
    const Product = app.models.Product;
    const User = app.models.User;
    const Feedback = app.models.Feedback;

    return function findAll(req, res, next) {
        Sell.findAll({
            where: { deletedAt: null },
            include: [ { model: Product, include: [ { model: User, as: 'seller' } ] }, { model: User, as: 'buyer' }, { model: Feedback } ]
        }).then((data) => {
            if (!data) return res.sendStatus(204);
            app.middlewares.tools.cache.set('Sell', data, req.originalUrl);
            res.send(data);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
