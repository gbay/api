module.exports = (app) => {
    return function findOne(req, res, next) {
        app.middlewares.tools.cache.set('Product', req.sell, req.originalUrl);
        res.send(req.sell);
    }
};
