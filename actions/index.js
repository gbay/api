module.exports = (app) => {
    console.log('Loading actions...');

    app.actions = {
        auctions: require('./auctions')(app),
        auth: require('./auth')(app),
        categories: require('./categories')(app),
        products: require('./products')(app),
        sells: require('./sells')(app),
        sellers: require('./sellers')(app),
        users: require('./users')(app)
    };
};