module.exports = (app) => {
    const Category = app.models.Category;

    return function findAll(req, res, next) {
        Category.findAll().then((data) => {
            if (!data) return res.sendStatus(204);
            app.middlewares.tools.cache.set('Categories', data, req.originalUrl);
            res.send(data);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
