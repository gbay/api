module.exports = (app) => {
    const Auction = app.models.Auction;
    const Bid = app.models.Bid;
    const Sell = app.models.Sell;
    const Product = app.models.Product;
    const User = app.models.User;

    return (req, res, next) => {
        Auction.findOne({
            where: { id: req.params.auctionId },
            include: [ { model: Bid, include: [ { model: User, as: 'buyer' } ] }, { model: Sell, include: [ { model: Product, include: [ { model: User, as: 'seller' } ] } ] } ]
        }).then((data) => {
            if (!data) return res.status(404).send('Auction not found');
            if (data.deletedAt !== null) return res.status(404).send('Auction deleted');
            req.auction = data;
            next();
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};