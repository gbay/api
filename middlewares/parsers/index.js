module.exports = (app) => {
    return {
        auction: require('./auction')(app),
        product: require('./product')(app),
        seller: require('./seller')(app),
        sell: require('./sell')(app)
    }
};