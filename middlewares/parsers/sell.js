module.exports = (app) => {
    const Sell = app.models.Sell;
    const Product = app.models.Product;
    const User = app.models.User;
    const Feedback = app.models.Feedback;

    return function findOne(req, res, next) {
        Sell.findOne({
            where: { id: req.params.sellId },
            include: [ { model: Product, include: [ { model: User, as: 'seller' } ] }, { model: User, as: 'buyer' }, { model: Feedback } ]
        }).then((data) => {
            if (!data) return res.status(404).send('Sell not found');
            if (data.deletedAt !== null) return res.status(404).send('Sell deleted');
            req.sell = data;
            next();
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
