module.exports = (app) => {
    const Product = app.models.Product;
    const Category = app.models.Category;
    const Sell = app.models.Sell;
    const User = app.models.User;
    const Auction = app.models.Auction;

    return (req, res, next) => {
        Product.findOne({
            where: { id: req.params.productId },
            include: [ { model: Category, as: 'categories' }, { model: User, as: 'seller' }, { model: Sell, required: false, where: { buyerId: null }, include: [ { model: Auction, required: false, where: { closedAt: null, deletedAt: null } } ] } ]
        }).then((data) => {
            if (!data) return res.status(404).send('Product not found');
            if (data.deletedAt !== null) return res.status(404).send('Product deleted');
            req.product = data;
            next();
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};