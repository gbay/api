module.exports = (app) => {
    const User = app.models.User;
    const Product = app.models.Product;
    const Evaluation = app.models.Evaluation;
    const Sell = app.models.Sell;
    const Feedback = app.models.Feedback;

    return function findOne(req, res, next) {
        User.findOne({
            where: { id: req.params.sellerId },
            include: [ { model: Product, required: true, include: [ { model: Sell, required: false, include: [ { model: Feedback, required: false, where: { deletedAt: null } } ] } ] }, { model: Evaluation, required: false, where: { deletedAt: null }  } ]
        }).then((seller) => {
            if (!seller) return res.status(404).send('Seller not found');
            if (seller.deletedAt !== null) return res.status(404).send('Seller account deleted');

            req.seller = seller;
            next();
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
