module.exports = (app) => {
    console.log('Loading cache...');
    const Cache = app.models.Cache;

    Cache.find({}, (err, data) => {
        if (err) console.log(err);
        app.cache = data;
        console.log(app.cache);
    });

    function getDataFromKey(key) {
        for (let item of app.cache) {
            const data = item.storage[key];
            if (data) return data;
        }

        return null;
    }

    function getItemFromModel(model) {
        for (let item of app.cache)
            if (item.model === model) return item;

        return null;
    }

    function get(req, res, next) {
        let url = stripTrailingSlash(req.originalUrl);
        const data = getDataFromKey(url);
        if (data) return res.send(data);

        next();
    }

    function instancesToJSON(instances) {
        let data;

        if (instances instanceof Array) {
            data = [];
            instances.forEach((instance) => {
                data.push(instance.get({ plain: true }));
            });
        } else {
            data = instances.get({ plain: true });
        }

        return data;
    }

    function stripTrailingSlash(str) {
        str = str.replace('//', '/');

        if(str.substr(-1) === '/') {
            return str.substr(0, str.length - 1);
        }

        return str;
    }

    function set(model, data, key) {
        key = stripTrailingSlash(key);
        data = instancesToJSON(data);

        let item = getItemFromModel(model);
        if (item) {
            item.storage[key] = data;
        } else {
            item = { model: model, storage: { } };
            item.storage[key] = data;
            app.cache.push(item);
        }

        Cache.update({ model: model }, item, { upsert: true }, (err) => {
            if (err) console.log(err);
        });
    }

    function clean(model) {
        for (let i = 0; i < app.cache.length; i++) {
            if(app.cache[i].model === model) {
                app.cache.splice(i, 1);
                 break;
            }
        }
    }

    return {
        get,
        set,
        clean
    }
};
