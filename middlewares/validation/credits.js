module.exports = (req , res, next) => {
    if (!req.body || !req.body.credits) {
        return res.status(400).send('Missing credits fields (credits)');
    }

    let credits = Number(req.body.credits);
    if (isNaN(credits) || credits < 5) {
        return res.status(400).send('Wrong credits value. (min: 5)');
    }

    return next();
};