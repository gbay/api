module.exports = (app) => {
    return {
        auction: require('./auction')(app),
        bid: require('./bid'),
        credits: require('./credits'),
        evaluation: require('./evaluation'),
        feedback: require('./feedback'),
        login: require('./login'),
        product: require('./product'),
        buy: require('./buy'),
        user: require('./user')
    }
};