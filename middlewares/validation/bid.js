module.exports = (req , res, next) => {
    if (!req.body || !req.body.amount) {
        return res.status(400).send('Missing bid fields (amount)');
    }

    let amount = Number(req.body.amount);
    if (isNaN(amount) || amount < 1) {
        return res.status(400).send('Wrong amount value. (min: 1)');
    }

    if (req.user.credits < amount) {
        return res.status(400).send(`Insufficient credits. (${amount} needed)`);
    }

    return next();
};