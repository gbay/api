module.exports = (req , res, next) => {
    if (!req.body || !req.body.comment) {
        return res.status(400).send('Missing evaluations fields (comment)');
    }

    return next();
};