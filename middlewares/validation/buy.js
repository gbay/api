module.exports = (req , res, next) => {
    if (!req.body || !req.body.quantity) {
        return res.status(400).send('Missing buy fields (quantity)');
    }

    let quantity = Number(req.body.quantity);
    if (isNaN(quantity) || quantity < 1) {
        return res.status(400).send('Wrong quantity value. (min: 1)');
    }

    return next();
};