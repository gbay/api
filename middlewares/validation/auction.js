module.exports = (app) => {
    return (req , res, next) => {
        if (!req.body || !req.body.end || !req.body.start || !req.body.basePrice || !req.body.productId || !req.body.quantity) {
            return res.status(400).send('Missing auction fields (end, basePrice, start, productId, quantity)');
        }

        let basePrice = Number(req.body.basePrice);
        if (isNaN(basePrice) || basePrice < 1) {
            return res.status(400).send('Wrong basePrice value. (min: 1)');
        }

        let quantity = Number(req.body.quantity);
        if (isNaN(quantity) || quantity < 1) {
            return res.status(400).send('Wrong quantity value. (min: 1)');
        }

        let end = Date.parse(req.body.end);
        if (isNaN(end)) {
            return res.status(400).send('Wrong end value.');
        }

        let start = Date.parse(req.body.start);
        if (isNaN(start)) {
            return res.status(400).send('Wrong start value.');
        }

        let productId = Number(req.body.productId);
        if (isNaN(productId)) {
            return res.status(400).send('Wrong productId value. (min: 1)');
        }

        next();
    }
};