module.exports = (req , res, next) => {
    if (!req.body || !req.body.note) {
        return res.status(400).send('Missing feedback fields (note, comment?)');
    }

    let note = Number(req.body.note);
    if (isNaN(note) || note < 0 || note > 5) {
        return res.status(400).send('Wrong note value. (min: 0, max: 5)');
    }

    if (req.body.comment) {
        req.body.comment = req.body.comment === '' ? null : req.body.comment;
    }

    return next();
};