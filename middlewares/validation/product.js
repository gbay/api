module.exports = (req , res, next) => {
    if (!req.body || !req.body.name || !req.body.price || !req.body.description) {
        return res.status(400).send('Missing product fields (name, price, description, categories)');
    }

    if (!req.body.categories instanceof Array) {
        return res.status(400).send('Wrong categories value. (array of integer expected)');
    }

    for (let i = 0; i < req.body.categories.length; i++) {
        let categoryId = Number(req.body.categories[i]);
        if (isNaN(categoryId) || categoryId < 1) {
            return res.status(400).send('Wrong categoryId value ' + req.body.categories[i] + '. (positive integer expected)');
        }
    }

    if (req.body.name.length < 2) {
        return res.status(400).send('Wrong name value. (min length 2)');
    }

    let price = Number(req.body.price);
    if (isNaN(price) || price < 1) {
        return res.status(400).send('Wrong price value. (min: 1)');
    }

    if (req.body.description.length < 5) {
        return res.status(400).send('Wrong description value. (min length 5)');
    }

    if (req.body.stock) {
        req.body.stock = isNaN(req.body.stock) ? 0 : Math.max(req.body.stock, 0);
    }

    next();
};