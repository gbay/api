module.exports = (app) => {
    return (req, res, next) => {
        req.seller.getEvaluations({ where: { buyerId: req.user.id } }).then((data) => {
            if (data.length > 0) return res.status(403).send('You already evaluated this seller');
            next();
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
