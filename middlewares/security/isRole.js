module.exports = (app) => {
    return (role) => {
        return (req, res, next) => {
            if (req.user.role.name !== role) {
                return res.status(403).send(`Insufficient privileges ('${role}' required)`);
            }
        }
    }
};
