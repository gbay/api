module.exports = (app) => {
    return (req, res, next) => {
        if (req.user.id === req.auction.sell.product.sellerId) {
            return res.status(403).send('Cannot bid on your own auction');
        }

        next();
    }
};
