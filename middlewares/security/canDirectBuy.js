module.exports = (app) => {
    return (req, res, next) => {
        if (!req.product.directBuy) {
            return res.status(401).send('Product cannot be directly bought');
        }

        if (req.product.stock !== null && req.product.stock - Number(req.body.quantity) < 0) {
            return res.status(401).send('Insufficient product stock');
        }

        if (req.product.price * Number(req.body.quantity) > req.user.credits) {
            return res.status(401).send('Insufficient credits');
        }

        next();
    }
};
