const jwtk = require('jsonwebtoken');

module.exports = (app) => {
    const Token = app.models.Token;
    const User = app.models.User;
    const Role = app.models.Role;
    const Evaluation = app.models.Evaluation;
    const Product = app.models.Product;
    const Sell = app.models.Sell;
    const Bid = app.models.Bid;

    return (req, res, next) => {
        if(!req.headers || !req.headers.authorization) return res.status(403).send('Authentication required');

        jwtk.verify(req.headers.authorization, app.settings.security.salt, null, (err, decryptedToken) => {
            if (err) return res.status(401).send('Invalid token');

            Token.findById(decryptedToken.tokenId, (err, token) => {
                if(err) return res.status(500).send(err);
                if(!token) return res.status(403).send('Invalid token');

                User.findOne({
                    where: { deletedAt: null, id: token.userId },
                    include: [ { model: Role }, { model: Evaluation }, { model: Product, include: [ { model: Sell } ] }, { model: Bid }, { model: Sell, as: 'purchases' } ]
                }).then((data) => {
                    if (!data) return res.status(403).send('No user attached to this token');
                    req.user = data;
                    next();
                }).catch((err) => {
                    res.status(500).send(err.message);
                });
            });
        });
    }
};
