module.exports = (app) => {
    return (req, res, next) => {
        if (req.user.id !== req.product.sellerId) {
            return res.status(403).send('Reserved to the product owner');
        }

        next();
    }
};
