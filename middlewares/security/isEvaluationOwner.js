module.exports = () => {
    return (req, res, next) => {
        if (req.user.id !== Number(req.params.buyerId)) {
            return res.status(403).send('Not your evaluation');
        }

        next();
    }
};
