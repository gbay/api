module.exports = (app) => {
    return (req, res, next) => {
        if (req.user.id !== req.sell.buyerId) {
            return res.status(403).send('Reserved to the buyer');
        }

        next();
    }
};
