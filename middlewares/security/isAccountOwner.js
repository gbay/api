module.exports = () => {
    return (req, res, next) => {
        if (req.user.id !== Number(req.params.userId)) {
            return res.status(403).send('Not your account');
        }

        next();
    }
};
