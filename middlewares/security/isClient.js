module.exports = (app) => {
    return (req, res, next) => {
        req.seller.getSells({ where: { buyerId: req.user.id }}).then((data) => {
            if (data.length === 0) return res.status(403).send('Reserved to the clients');
            next();
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
