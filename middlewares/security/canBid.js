module.exports = (app) => {
    return (req, res, next) => {
        if (req.auction.basePrice > Number(req.body.amount)) {
            return res.status(401).send(`Bid too small (minimum basePrice is ${req.auction.basePrice})`);
        }

        if (req.auction.closedAt !== null) {
            return res.status(401).send(`Auction closed`);
        }

        let lastBidAmount = req.auction.bids.length ? req.auction.bids[req.auction.bids.length - 1].amount : 0;
        if (lastBidAmount >= Number(req.body.amount)) {
            return res.status(401).send(`Bid too small (minimum ${lastBidAmount + 1})`);
        }

        next();
    }
};
