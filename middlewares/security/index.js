module.exports = (app) => {
    return {
        isAccountOwner: require('./isAccountOwner')(app),
        isRole: require('./isRole')(app),
        isAuthenticated: require('./isAuthenticated')(app),
        isEvaluationOwner: require('./isEvaluationOwner')(app),
        isProductOwner: require('./isProductOwner')(app),
        isNotProductOwner: require('./isNotProductOwner')(app),
        isNotAuctionOwner: require('./isNotAuctionOwner')(app),
        isBuyer: require('./isBuyer')(app),
        isClient: require('./isClient')(app),
        canDirectBuy: require('./canDirectBuy')(app),
        canBid: require('./canBid')(app),
        hasNotEvaluated: require('./hasNotEvaluated')(app)
    }
};