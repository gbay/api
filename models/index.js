module.exports = (app) => {
    console.log('Loading models...');

    app.models = {
        Role : require('./mysql/role')(app),
        Bid: require('./mysql/bid')(app),
        Category: require('./mysql/category')(app),
        Evaluation: require('./mysql/evaluation')(app),
        Feedback : require('./mysql/feedback')(app),
        Auction : require('./mysql/auction')(app),
        Sell : require('./mysql/sell')(app),
        User : require('./mysql/user')(app),
        Product: require('./mysql/product')(app),
        Cache: require('./mongodb/cache')(app),
        Token: require('./mongodb/token')(app)
    };

    app.models.Auction.belongsTo(app.models.Sell);
    app.models.Auction.hasMany(app.models.Bid);
    app.models.Bid.belongsTo(app.models.Auction);
    app.models.Bid.belongsTo(app.models.User, { as: 'buyer', foreignKey: 'buyerId' });
    app.models.Category.belongsTo(app.models.Category, { as: 'parent', foreignKey: 'parentId' });
    app.models.Evaluation.belongsTo(app.models.User, { as: 'buyer', foreignKey: 'buyerId' });
    app.models.Feedback.belongsTo(app.models.Sell);
    app.models.Product.belongsToMany(app.models.Category, { through: 'ProductCategory', as: 'categories' });
    app.models.Product.hasMany(app.models.Sell);
    app.models.Product.belongsTo(app.models.User, { as: 'seller', foreignKey: 'sellerId' });
    app.models.Sell.hasOne(app.models.Feedback);
    app.models.Sell.hasOne(app.models.Auction);
    app.models.Sell.belongsTo(app.models.Product);
    app.models.Sell.belongsTo(app.models.User, { as: 'buyer', foreignKey: 'buyerId' });
    app.models.User.belongsTo(app.models.Role);
    app.models.User.hasMany(app.models.Evaluation, { foreignKey: 'sellerId' });
    app.models.User.hasMany(app.models.Product, { foreignKey: 'sellerId' });
    app.models.User.hasMany(app.models.Bid, { foreignKey: 'buyerId' });
    app.models.User.hasMany(app.models.Sell, { as: 'purchases', foreignKey: 'buyerId' });
};
