const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('product', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        price: {
            type: Sequelize.DECIMAL(10, 2),
            allowNull: false
        },
        description: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        stock: {
            type: Sequelize.INTEGER,
            defaultValue: 0,
            allowNull: true
        },
        directBuy: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        }
    }, {
        paranoid: true,
        hooks: {
            beforeDestroy: (product) => {
                product.sells.forEach((sell) => {
                    if (!sell.buyerId) {
                        sell.destroy();
                    }
                });
            }
        }
    });
};
