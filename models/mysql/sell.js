const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('sell', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        quantity: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    }, {
        paranoid: true,
        hooks: {
            beforeDestroy: (sell) => {
                sell.getAuction().then((auction) => {
                    if (auction) {
                        auction.destroy();
                    }
                }).catch((err) => {
                    console.log(err.message);
                });
            }
        }
    });
};
