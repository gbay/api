const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('evaluation', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        comment: {
            type: Sequelize.TEXT,
            allowNull: false
        }
    }, { paranoid: true });
};
