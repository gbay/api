const Sequelize = require('sequelize');

module.exports = (app) =>{
    return app.sequelize.define('auction', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        start: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            allowNull: false
        },
        end: {
            type: Sequelize.DATE,
            allowNull: false
        },
        basePrice: {
            type: Sequelize.DOUBLE,
            allowNull: false
        },
        closedAt: {
            type: Sequelize.DATE,
            allowNull: true
        }
    }, {
        paranoid: true,
        hooks: {
            beforeDestroy: (auction) => {
                auction.getBids().then((bids) => {
                    bids.forEach((bid) => {
                        bid.destroy();
                    })
                }).catch((err) => {
                    console.log(err.message);
                });
            }
        }
    });
};
