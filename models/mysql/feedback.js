const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('feedback', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        note: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        comment: {
            type: Sequelize.TEXT('tiny'),
            allowNull: false
        }
    }, { paranoid: true });
};
