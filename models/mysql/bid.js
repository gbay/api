const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('bid', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        amount: {
            type: Sequelize.DECIMAL(10, 2),
            allowNull: false
        }
    }, { paranoid: true,
        hooks: {
            beforeDestroy: (bid) => {
                bid.getBuyer().then((buyer) => {
                    buyer.increment({ credits: bid.amount});
                }).catch((err) => {
                    console.log(err.message);
                });
            }
        }
    });
};
