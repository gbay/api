const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('user', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        credits: {
            type: Sequelize.DECIMAL(10, 2),
            defaultValue: 0.00,
            allowNull: false
        }
    }, {
        paranoid: true,
        hooks: {
            beforeDestroy: (user) => {
                user.products.forEach((product) => {
                    product.destroy();
                });
            }
        }
    });
};
