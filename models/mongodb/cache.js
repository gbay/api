const Schema = require('mongoose').Schema;
const ttl = require('mongoose-ttl');

module.exports = (app) => {
    const schema = new Schema({
        model: {
            type: String
        },
        storage: {
            type: Schema.Types.Mixed
        }
    });

    schema.plugin(ttl, {ttl: '1d'});
    return app.mongoose.model('Cache', schema);
};