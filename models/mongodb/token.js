const Schema = require('mongoose').Schema;
const ttl = require('mongoose-ttl');

module.exports = (app) => {
    const schema = new Schema({
        userId: {
            type: Schema.Types.Number
        }
    });

    schema.plugin(ttl, {ttl: '1d'});
    return app.mongoose.model('Token', schema);
};