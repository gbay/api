const sha1 =  require('sha1');

module.exports = (app) => {
    console.log('Loading fixtures...');

    const Auction = app.models.Auction;
    const Bid = app.models.Bid;
    const Category = app.models.Category;
    const Evaluation = app.models.Evaluation;
    const Feedback = app.models.Feedback;
    const Product =  app.models.Product;
    const Role = app.models.Role;
    const Sell = app.models.Sell;
    const User = app.models.User;

    app.sequelize.sync().then(() => {
        Role.findOrCreate({ where: { name: "Administrateur" } });
        Role.findOrCreate({ where: { name: "Utilisateur" } });

        Category.findOrCreate({ where: { name: "Livres" } });
        Category.findOrCreate({ where: { name: "Musique" } });
        Category.findOrCreate({ where: { name: "Jeux vidéo" } });
        Category.findOrCreate({ where: { name: "High-Tech" } });
        Category.findOrCreate({ where: { name: "Jouets" } });
        Category.findOrCreate({ where: { name: "Maison" } });
        Category.findOrCreate({ where: { name: "Beauté" } });
        Category.findOrCreate({ where: { name: "Vêtements" } });
        Category.findOrCreate({ where: { name: "Bijoux" } });
        Category.findOrCreate({ where: { name: "Sports" } });
        Category.findOrCreate({ where: { name: "Automobile" } });
        Category.findOrCreate({ where: { name: "Drame", parentId: 1 } });
        Category.findOrCreate({ where: { name: "Science-fiction", parentId: 1 } });
        Category.findOrCreate({ where: { name: "Thriller", parentId: 1 } });
        Category.findOrCreate({ where: { name: "Electro", parentId: 2 } });
        Category.findOrCreate({ where: { name: "Rock", parentId: 2 } });
        Category.findOrCreate({ where: { name: "Rap", parentId: 2 } });
        Category.findOrCreate({ where: { name: "Plate-forme", parentId: 3 } });
        Category.findOrCreate({ where: { name: "FPS", parentId: 3 } });
        Category.findOrCreate({ where: { name: "RPG", parentId: 3 } });
        Category.findOrCreate({ where: { name: "Ordinateur", parentId: 4 } });
        Category.findOrCreate({ where: { name: "Téléphone", parentId: 4 } });
        Category.findOrCreate({ where: { name: "Tablette", parentId: 4 } });
        Category.findOrCreate({ where: { name: "Médicament", parentId: 7 } });
        Category.findOrCreate({ where: { name: "Maquillage", parentId: 7 } });
        Category.findOrCreate({ where: { name: "Chaussures", parentId: 8 } });
        Category.findOrCreate({ where: { name: "Pantalon", parentId: 8 } });
        Category.findOrCreate({ where: { name: "Pullover", parentId: 8 } });

        User.findOrCreate({ where: { email: "pouet@test.test", password: sha1("pouet123"), name: "Pouet TEST", roleId: 1 } });
        User.findOrCreate({ where: { email: "yolo@test.test", password: sha1("yolo123"), name: "Yolo TEST", roleId: 2 } });
        User.findOrCreate({ where: { email: "foo@test.test", password: sha1("foo123"), name: "Foo TEST", roleId: 2 } });
        User.findOrCreate({ where: { email: "bar@test.test", password: sha1("bar123"), name: "Bar TEST", roleId: 2 } });
        User.findOrCreate({ where: { email: "baz@test.test", password: sha1("baz123"), name: "Baz TEST", roleId: 2 } });

        Product.findOrCreate({ where: { name: "iPhone 7", description: "Dernier iPhone pour pas cher !", price: 599.99, directBuy: true, sellerId: 2 }, defaults: { stock: 1 } }).spread((product) => { product.setCategories([4, 19]) });
        Product.findOrCreate({ where: { name: "Call of Duty licence", description: "Code d'activation pour Call of Duty à prix réduit", price: 9.99, directBuy: false, sellerId: 2 }, defaults: { stock: 14 } }).spread((product) => { product.setCategories([3, 16]) });
        Product.findOrCreate({ where: { name: "Stan Smith white", description: "Paire de Stan Smith blanches", price: 99.99, directBuy: true, sellerId: 3 }, defaults: { stock: null } }).spread((product) => { product.setCategories([8, 26]) });
        Product.findOrCreate({ where: { name: "Rouge a lèvre Dior", description: "Rouge à lèvre Dior couleur rose bonbon avec des paillettes", directBuy: false, price: 24.50, sellerId: 3 }, defaults: { stock: null } }).spread((product) => { product.setCategories([8, 26]) });
        Product.findOrCreate({ where: { name: "50 Nuances de Grey", description: "Roman dramatique 50 nuances de Grey en bon état", price: 10.00, directBuy: false, sellerId: 3 }, defaults: { stock: 0 } }).spread((product) => { product.setCategories([1, 12]) });

        Evaluation.findOrCreate({ where: { comment: "Attention ! Arnaqueur !", buyerId: 5, sellerId: 2 } });
        Evaluation.findOrCreate({ where: { comment: "Il est trop cool ce gars là il vend plein de trucs !", buyerId: 4, sellerId: 3 } });
        Evaluation.findOrCreate({ where: { comment: "Au top ! Il a tout ce dont on a besoin :)", buyerId: 5, sellerId: 3 } });

        Sell.findOrCreate({ where: { quantity: 1, productId: 2, buyerId: 5 } });
        Sell.findOrCreate({ where: { quantity: 2, productId: 3, buyerId: 4 } });
        Sell.findOrCreate({ where: { quantity: 1, productId: 5, buyerId: 5 } });
        Sell.findOrCreate({ where: { quantity: 1, productId: 3, buyerId: null } });

        Feedback.findOrCreate({ where: { note: 0, comment: "Je n'ai jamais reçu la clé d'activation, c'est une arnaque !", sellId: 1 } });
        Feedback.findOrCreate({ where: { note: 10, comment: "Chaussures état neuf, pas de contrefaçon, reçues sous 3 jours. Impec' !", sellId: 2 } });
        Feedback.findOrCreate({ where: { note: 8, comment: "Vente parfaite, le bouquin était un peu corné ceci dit", sellId: 3 } });

        Auction.findOrCreate({ where: { start: "2017-02-10 00:00:00", end: "2017-02-17 00:00:00", basePrice: 5.00, sellId: 3 } });
        Auction.findOrCreate({ where: { start: "2017-04-03 00:00:00", end: "2017-05-03 00:00:00", basePrice: 10.00, sellId: 4 } });

        Bid.findOrCreate({ where: { amount: 5, auctionId: 1, buyerId: 4 } });
        Bid.findOrCreate({ where: { amount: 6, auctionId: 1, buyerId: 5 } });
        Bid.findOrCreate({ where: { amount: 9, auctionId: 1, buyerId: 4 } });
        Bid.findOrCreate({ where: { amount: 10, auctionId: 1, buyerId: 2 } });
        Bid.findOrCreate({ where: { amount: 11, auctionId: 1, buyerId: 5 } });

        Bid.findOrCreate({ where: { amount: 10, auctionId: 2, buyerId: 4 } });
        Bid.findOrCreate({ where: { amount: 12, auctionId: 2, buyerId: 5 } });
        Bid.findOrCreate({ where: { amount: 15, auctionId: 2, buyerId: 4 } });
    });
};
