const app = require('express')();

require('./settings')(app);                             // Loading settings
require('./databases')(app)                             // Loading databases connections (promise)
    .then(() => { require('./models')(app) })           // Loading models
    .then(() => { require('./boot')(app)} )             // Loading fixtures + cache + logger
    .then(() => { require('./middlewares')(app) })      // Loading middlewares
    .then(() => { require('./actions')(app) })          // Loading actions
    .then(() => { require('./tasks')(app) })            // Loading tasks
    .then(() => { require('./routes')(app) })           // Loading routes
    .then(() => { console.log(`gBay API listening on port ${app.settings.port}\n`); app.listen(app.settings.port); })
    .catch((err) => { console.log(err) });
